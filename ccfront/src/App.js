import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ResultsTable from './ResultsTable.js'
class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>BioCore Code Challenge</h2>
        </div>
        <p className="App-intro">
          <code>Find the longest sequence in a list</code>
        </p>
        <ResultsTable url='results.csv' />
      </div>
    );
  }
}

export default App;
