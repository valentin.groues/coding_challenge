import React, {Component} from 'react';
import './ResultsTable.css';

class ResultsTable extends Component {

    constructor(props) {
        super(props);
        const {url} = props;
        var Papa = require("papaparse/papaparse.min.js");
        this.state = {};
        Papa.parse(url, {
            download: true,
            complete: (results) => this.gotResults(results)
        });
    }

    gotResults(results) {
        let fastestProject;
        let fastestTime;
        for (let i = 0; i < results.data.length; i++) {
            const time = parseFloat(results.data[i][2]);
            if (!fastestTime || time < fastestTime) {
                fastestTime = time;
                fastestProject = results.data[i][1];
            }

        }
        this.setState({results, fastestProject, fastestTime});
    }

    render() {
        const {results, fastestProject, fastestTime} = this.state;
        const data = results === undefined ? [] : results.data;
        return (
            <div id="ResultsTable">
                <div>
                    <div className="first-place">1</div>
                    <div className="second-place">2</div>
                    <div className="third-place">3</div>
                </div>
                {fastestTime && <h2>Fastest is {fastestProject} in {fastestTime.toFixed(2)} ms</h2>}
                {!fastestTime && <h2>No results yet</h2>}
                {fastestTime &&
                <table>
                    <thead>
                    <tr>
                        <th>Project ID</th>
                        <th>Project Name</th>
                        <th>Time</th>
                        <th>Success</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((entry, entryIndex) => {
                        if (entry[0]) {
                            let success = true;
                            let time;
                            try {
                                time = parseFloat(entry[2]).toFixed(2);

                            } catch (e) {
                                success = false;
                            }
                            return (
                                <tr key={entryIndex}>
                                    <td>{entry[0]}</td>
                                    <td>{entry[1]}</td>
                                    <td>{success && time}</td>
                                    <td>{success && 'yes'}{!success && 'no'}</td>
                                </tr>)
                        } else {
                            return null;
                        }
                    })}
                    </tbody>
                </table>}
            </div>
        );
    }
}

export default ResultsTable;