gitlab -c python-gitlab.cfg project-fork list --project-id 1526 | sed '/^[ \t]*$/d' | while read line; 
do 
project_id=`echo $line | cut -d":" -f2 | tr -d '[:space:]'`;
fork_url=`gitlab -o json -f http_url_to_repo -c python-gitlab.cfg project get --id $project_id | cut -f4 -d'"'`; 
git clone $fork_url $project_id; 
cd $project_id;
# virtualenv venv;
#source venv/bin/activate;
#if test -f "requirements.txt"; then
#    pip install -r requirements.txt;
#fi
cp ../evaluate.py .;
project_name=`gitlab -o json -f name_with_namespace -c ../python-gitlab.cfg project get --id $project_id | cut -f4 -d'"'`
echo -n $project_id >> ../results.csv;
echo -n ',' >> ../results.csv;
echo -n $project_name >> ../results.csv;
echo -n ',' >> ../results.csv;
echo `python evaluate.py` >> ../results.csv;
done
