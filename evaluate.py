import logging
from random import randint
from timeit import timeit

from challenge import candidate_method

logger = logging.getLogger(__name__)


def reference(sequence):
    sequence_set = set(sequence)
    visited_number = {}
    max_length = 0
    for i in sequence:
        if visited_number.get(i):
            continue
        visited_number[i] = True
        length = 1
        forward = i + 1
        while forward in sequence_set:
            forward += 1
            length += 1
            visited_number[forward] = True
        backward = i - 1
        while backward in sequence_set:
            backward -= 1
            length += 1
            visited_number[backward] = True
        max_length = max(max_length, length)
    return max_length


sequence = [randint(0, 100000) for i in range(1000000)]
assert reference(sequence) == candidate_method(sequence)
print(timeit('max([candidate_method([randint(0, 100000) for i in range(10000000)]) for i in range(1)])',
             setup="from __main__ import candidate_method; from random import randint", number=1))
